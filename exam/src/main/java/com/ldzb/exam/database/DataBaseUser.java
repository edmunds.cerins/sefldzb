package com.ldzb.exam.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DataBaseUser {
    public static void main(String[] args) {
        String text = "ldzb_db.users";
        try {

            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ldzb_db", "root", "MasterRoot_77");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + text);
            while (rs.next())
                System.out.println(rs.getString(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
