package com.ldzb.exam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {
    @RequestMapping(value = "/start")
    public String start() {
        return "start";
    }

    @RequestMapping(value = "/randomise")
    public String randomise() {
        return "randomise";
    }
}

